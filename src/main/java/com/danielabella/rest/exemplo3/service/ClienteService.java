package com.danielabella.rest.exemplo3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.danielabella.rest.exemplo3.domain.Cliente;
import com.danielabella.rest.exemplo3.repository.ClienteRepository;

@Service
@Validated
public class ClienteService {

	@Autowired
	private ClienteRepository repository;
	
	public Cliente buscarCliente(String cliente) {
		return repository.findOne(cliente);
	}

	public List<Cliente> listarTodosClientes() {
		return repository.findAll();
	}
	
	@Transactional
	public Cliente inserirCliente(Cliente cliente) {
		return repository.save(cliente);
	}

	public ClienteRepository getRepository() {
		return repository;
	}

	public void setRepository(ClienteRepository repository) {
		this.repository = repository;
	}
	
}
