package com.danielabella.rest.exemplo3.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.danielabella.rest.exemplo3.domain.Serasa;
import com.danielabella.rest.exemplo3.repository.SerasaRepository;

@Service
@Validated
public class SerasaService {

	@Autowired
	private SerasaRepository repository;
	
	
	public Serasa buscarDevedorPorCpf(String cpfDevedor) {
		return repository.findOne(cpfDevedor);
	}
	
	public List<Serasa> listarDevedores() {
		return repository.findAll();
	}
	
	@Transactional
	public Serasa inserirDevedor(Serasa serasa) {
		return repository.save(serasa);
	}
	
	public SerasaRepository getRepository() {
		return repository;
	}

	public void setRepository(SerasaRepository repository) {
		this.repository = repository;
	}
	
}
