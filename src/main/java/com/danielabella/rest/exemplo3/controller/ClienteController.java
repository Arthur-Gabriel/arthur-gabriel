package com.danielabella.rest.exemplo3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.danielabella.rest.exemplo3.domain.Cliente;
import com.danielabella.rest.exemplo3.service.ClienteService;

@RestController
public class ClienteController {

	@Autowired
	private ClienteService clienteService;
	
	@RequestMapping(value = "/cliente", method = RequestMethod.GET)
	public ResponseEntity< List<Cliente> > listarTodosCliente() {

		List<Cliente> listaClientes = clienteService.listarTodosClientes();

		return new ResponseEntity< List<Cliente> >(listaClientes, HttpStatus.OK);
	}
	

	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public ResponseEntity<Cliente> obterUsuario(@PathVariable String cpf) {

		Cliente cliente = clienteService.buscarCliente(cpf);

		if (cliente == null) {
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/cliente", method = RequestMethod.POST)
	public ResponseEntity<Cliente> inserirCliente(@RequestBody Cliente cliente) {

		try {
			Cliente clienteInserido = clienteService.inserirCliente(cliente);
			return new ResponseEntity<Cliente>(clienteInserido, HttpStatus.CREATED);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Cliente>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
