package com.danielabella.rest.exemplo3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.danielabella.rest.exemplo3.domain.Serasa;
import com.danielabella.rest.exemplo3.service.SerasaService;

@RestController
public class SerasaController {

	@Autowired
	private SerasaService serasaService;
	
	
	@RequestMapping(value = "/devedor", method = RequestMethod.GET)
	public ResponseEntity< List<Serasa> > listarDevedores() {

		List<Serasa> listaDevedores = serasaService.listarDevedores();

		return new ResponseEntity< List<Serasa> >(listaDevedores, HttpStatus.OK);
	}

	@RequestMapping(value = "/devedor/{cpf}", method = RequestMethod.GET)
	public ResponseEntity<Serasa> obterUsuario(@PathVariable String cpf) {

		Serasa serasa = serasaService.buscarDevedorPorCpf(cpf);

		if (serasa == null) {
			return new ResponseEntity<Serasa>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Serasa>(serasa, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/serasa", method = RequestMethod.POST)
	public ResponseEntity<Serasa> criarUsuario(@RequestBody Serasa serasa) {

		try {
			Serasa devedorInserido = serasaService.inserirDevedor(serasa);
			return new ResponseEntity<Serasa>(devedorInserido, HttpStatus.CREATED);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Serasa>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public SerasaService getSerasaService() {
		return serasaService;
	}

	public void setUserService(SerasaService serasaService) {
		this.serasaService = serasaService;
	}

	
	
}
