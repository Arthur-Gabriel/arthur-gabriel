package com.danielabella.rest.exemplo3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.danielabella.rest.exemplo3.domain.Conta;
import com.danielabella.rest.exemplo3.service.ContaService;

public class ContaController {


	@Autowired
	private ContaService contaService;

	@RequestMapping(value = "/conta", method = RequestMethod.GET)
	public ResponseEntity< List<Conta> > listarContas() {

		List<Conta> listaContas = contaService.listarTodasContas();

		return new ResponseEntity< List<Conta> >(listaContas, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/conta/{id}", method = RequestMethod.GET)
	public ResponseEntity<Conta> obterConta(@PathVariable String conta) {

		Conta conta1 = contaService.buscarConta(conta);

		if (conta == null) {
			return new ResponseEntity<Conta>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Conta>(conta1, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "/conta", method = RequestMethod.POST)
	public ResponseEntity<Conta> criarConta(@RequestBody Conta conta) {

		try {
			Conta contaCriada = contaService.criarConta(conta);
			return new ResponseEntity<Conta>(contaCriada, HttpStatus.CREATED);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Conta>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	public ContaService getContaService() {
		return contaService;
	}

	public void setContaService(ContaService contaService) {
		this.contaService = contaService;
	}
	
}
