package com.danielabella.rest.exemplo3.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.danielabella.rest.exemplo3.domain.Serasa;

public interface SerasaRepository extends JpaRepository<Serasa, String>{

}
