package com.danielabella.rest.exemplo3.domain;

public class Conta {

	private int agencia; 
	private int conta; 
	private String titular;
	private String cpf; 
	private double saldo; 
	private String tipoConta;
	
	public Conta() {
	}

	public Conta(int agencia, int conta, String titular, String cpf, double saldo, String tipoConta) {
		super();
		this.agencia = agencia;
		this.conta = conta;
		this.titular = titular;
		this.cpf = cpf;
		this.saldo = saldo;
		this.tipoConta = tipoConta;
	}

	public int getAgencia() {
		return agencia;
	}

	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}

	public int getConta() {
		return conta;
	}

	public void setConta(int conta) {
		this.conta = conta;
	}

	public String getTitular() {
		return titular;
	}

	public void setTitular(String titular) {
		this.titular = titular;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public String getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(String tipoConta) {
		this.tipoConta = tipoConta;
	}
	
	
	
}
