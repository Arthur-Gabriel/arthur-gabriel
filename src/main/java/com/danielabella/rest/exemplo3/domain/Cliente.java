package com.danielabella.rest.exemplo3.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Cliente implements Serializable{
	
	private static final long serialVersionUID = -7799369695818057571L;

	@Id
	private String nome;
	private String cpf;
	private String endereço;
	private String cep;
	
	public Cliente() {
	}
	
	public Cliente (String nome, String cpf, String endereço, String cep) {
		this.nome = nome; 
		this.cpf = cpf;
		this.endereço = endereço;
		this.cep = cep;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getEndereço() {
		return endereço;
	}
	public void setEndereço(String endereço) {
		this.endereço = endereço;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	
	
	
}
