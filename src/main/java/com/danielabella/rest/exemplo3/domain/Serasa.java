package com.danielabella.rest.exemplo3.domain;

public class Serasa {

	
	private String cpfDevedor; 
	private String nomeDevedor;
	private String cnpjEmpresa;
	private String nomeEmpresa;
	private String motivo;
	
	public Serasa() {
	}
	
	public Serasa(String cpfDevedor, String nomeDevedor, String cnpjEmpresa, String nomeEmpresa, String motivo) {
		super();
		this.cpfDevedor = cpfDevedor;
		this.nomeDevedor = nomeDevedor;
		this.cnpjEmpresa = cnpjEmpresa;
		this.nomeEmpresa = nomeEmpresa;
		this.motivo = motivo;
	}

	public String getCpfDevedor() {
		return cpfDevedor;
	}

	public void setCpfDevedor(String cpfDevedor) {
		this.cpfDevedor = cpfDevedor;
	}

	public String getNomeDevedor() {
		return nomeDevedor;
	}

	public void setNomeDevedor(String nomeDevedor) {
		this.nomeDevedor = nomeDevedor;
	}

	public String getCnpjEmpresa() {
		return cnpjEmpresa;
	}

	public void setCnpjEmpresa(String cnpjEmpresa) {
		this.cnpjEmpresa = cnpjEmpresa;
	}

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	
	
	
	
	
}
